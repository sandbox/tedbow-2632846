<?php

/**
 * @file
 * Contains \Drupal\calendar_demo\Controller\GameController.
 */

namespace Drupal\calendar_demo;

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;

/**
 * Class GameController.
 *
 * @package Drupal\calendar_demo\Controller
 */
class GameCreator {
  /**
   * Create Games.
   *
   * @return string
   *   Return Hello string.
   */
  function createGames() {
    $menu_link = MenuLinkContent::create([
      'title' => 'Game Calendar Demo',
      'link' => 'internal:/games-calendar/month/201604',
      'menu_name' => 'main',
    ]);
    $menu_link->save();

    /** @var \Drupal\taxonomy\Entity\Vocabulary $vocab */
    $vocab = Vocabulary::load('game_location');
    if (empty($vocab)) {
      $vocab = Vocabulary::create(
        [
          'vid' => 'game_location',
          'name' => 'Game Location',
        ]
      );
      $vocab->save();
    }

    $term_names = [
      'away' => 'Away',
      'home' => 'Home'
    ];
    /** @var \Drupal\taxonomy\Entity\Term[]*/
    $terms = [];
    foreach ($term_names as $term_key => $term_name) {
      $term = Term::create([
        'name' => $term_name,
        'vid' => $vocab->id(),
      ]);
      $term->save();
      $terms[$term_key] = $term;
    }


    $file_path = drupal_get_path('module', 'calendar_demo') . '/games.json';
    if (is_file($file_path)) {
      $json = file_get_contents($file_path);
      $games = json_decode($json);
      $game_count = 0;
      foreach ($games as $game) {
        $game_count++;
        $this->createGameNode($game, $terms);
      }

    }
    return $game_count;
  }

  /**
   * @param array $game
   * @param \Drupal\taxonomy\Entity\Term[] $terms
   */
  private function createGameNode($game, $terms) {
    $title = $game->title;
    if (stripos($title, 'at Red Sox') !== FALSE) {
      $game->tid = $terms['home']->id();
    }
    else {
      $game->tid = $terms['away']->id();
    }
    $date = new \DateTime($game->start_date . ' ' . $game->start_time);
    $game->time = substr($date->format('c'), 0, 19);

    $node_values = [
      'type' => 'game',
      'title' => $title ,
      'langcode' => 'en',
      'uid' => 1,
      'status' => 1,
      'body' => array('The body text'),
      'field_game_time' => [$game->time],
      'field_location' => [$game->tid],
    ];
    $node = Node::create(
      $node_values
    );
    $node->save();
  }
}
